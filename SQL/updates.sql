ALTER TABLE dbo.Customer
ALTER COLUMN CUPhonNo nvarchar(50)


/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */

USE [supportDEVELOPMENT]
GO
/****** Object:  StoredProcedure [WholesaleHunterCom].[sLoginVerify]    Script Date: 12/6/2016 9:12:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************************************************************
* Author:		Steve Henderson
* Create Date:	2016-09-26
* Purpose:		Check the username and password for correctness
* History:		
***********************************************************************************************/

ALTER PROCEDURE [WholesaleHunterCom].[sLoginVerify] (@Username nvarchar(50), @Password nvarchar(50))
WITH EXECUTE AS 'WHReadOnly'
AS

/*
exec [WholesaleHunterCom].[sLoginVerify] N'meven@satx.rr.com', N'20even07'
*/

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

begin try

DECLARE @CustomerID int

SELECT			@CustomerID=ID 
FROM			Customer c
WHERE			c.CUEmail=@Username
 AND			c.CUWebPW=@Password
 COLLATE Latin1_General_CS_AS

 IF @CustomerID is null
	SET @CustomerID=0

SELECT @CustomerID AS CustomerID

end try
begin catch
	select 'Unknown error'
end catch

/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */

USE [supportDEVELOPMENT]
GO
/****** Object:  StoredProcedure [WholesaleHunterCom].[iLoginCreate]    Script Date: 12/9/2016 12:01:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************************************************************
* Author:		Steve Henderson
* Create Date:	2016-09-26
* Purpose:		Create a new login
* History:		
***********************************************************************************************/

ALTER PROCEDURE [WholesaleHunterCom].[iLoginCreate] (@Username nvarchar(50), @Password nvarchar(50), @FirstName varchar(50), @LastName varchar(50), @Address1 nvarchar(30), @Address2 nvarchar(30), @City nvarchar(30), @State nvarchar(2), @ZIPCode nvarchar(5), @Phone nvarchar(50))
AS

/*
exec [WholesaleHunterCom].[iLoginCreate] N'steven2@hendersondatasolutions.com', N'Test', 'Steven', 'Henderson', N'1230 Peachtree St NE', N'Suite 1900 PMB 200', N'Atlanta', N'GA', N'30309', N'800-595-0437'
*/

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

begin try

DECLARE @CustomerID int

IF EXISTS (SELECT * FROM Customer c WHERE c.CUEmail=@Username)
BEGIN
	SELECT -1
END
ELSE
BEGIN

BEGIN TRAN

INSERT INTO	Customer (CUEmail, CUWebPW, BillFirstName, BillLastName, CUShAdr1, CUshAdr2, CUShCity, CUShState, CUShZipCd, CUPhonNo)
SELECT @Username, @Password, @FirstName, @LastName, @Address1, @Address2, @City, @State, @ZIPCode, @Phone

COMMIT

SELECT SCOPE_IDENTITY()

END

end try
begin catch
	ROLLBACK
	select 'Unknown error'
end catch

/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */
/* *********************************************************************** */

USE [supportDEVELOPMENT]
GO
/****** Object:  StoredProcedure [WholesaleHunterCom].[sProductSearchResults]    Script Date: 12/12/2016 5:50:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************************************************************
* Author:		Steve Henderson
* Create Date:	2016-09-19
* Purpose:		List all products and search information for specified criteria including data to populate various filters
* History:		
***********************************************************************************************/

ALTER PROCEDURE [WholesaleHunterCom].[sProductSearchResults] (
	@CategoryID int, 
	@MinPrice decimal(19,4), 
	@MaxPrice decimal(19,4), 
	@BrandID int, 
	@InStockOnly bit, 
	@NewOnly bit, 
	@Keywords varchar(8000), 
	@SortBy varchar(100), 
	@StartRow smallint, 
	@EndRow smallint)
WITH EXECUTE AS 'WHReadOnly'
AS
--10,710,216,618,106
/*
exec [WholesaleHunterCom].[sProductSearchResults] 219, null, 10, null, 1, 0, '', 'Price Low to High', 1, 15
*/

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--begin try

DECLARE @KeywordString varchar(8000)
SET @KeywordString=dbo.StringReplaceNonAlphaNum(@Keywords) --Remove all non-alphanumeric characters.  This is mainly to protect against SQL injection
SELECT @KeywordString=REPLACE(@Keywords,' ', ' AND ') --looks for every keyword provided

IF LEN(LTRIM(@KeywordString))=0 
	SET @KeywordString='""' --Prevents syntax errors

DECLARE @Resultset AS MyTable

DECLARE @IsCategoryParent bit, @ParentCategoryID int
SELECT @ParentCategoryID = (SELECT MAX(ParentID) FROM Category WHERE ID=@CategoryID)
SELECT @IsCategoryParent = CASE WHEN @ParentCategoryID=1 THEN 1 ELSE 0 END
IF @IsCategoryParent = 1
	SET @ParentCategoryID=@CategoryID

--Gather search results
DECLARE @SQL nvarchar(max)
SET @SQL=
'SELECT				 pm.ID AS ProductID
					,pm.GroupID
					,pm.CategoryID
					,pm.MfgID'
IF @CategoryID IN(11,12,13,14) --Ammunition
	SET @SQL = @SQL + ', Caliber, BulletType, Rounds, dbo.RemoveNonNumericCharacters(Weight)'
IF @CategoryID=15 --Shells
	SET @SQL = @SQL + ', Gauge, Length, [Shot Size], [Shot Weight], Power'
IF @CategoryID=219 --Bullets
	SET @SQL = @SQL + ', Caliber'
SET @SQL = @SQL + ' FROM				ProductMaster_T pm'

IF @MinPrice is not null OR @MaxPrice is not null OR @InStockOnly=1
SET @SQL=@SQL + ' LEFT JOIN			(SELECT GroupID, MIN(WHPrice) AS Price FROM ProductDetail pd WHERE Active=1 AND Qty>0 GROUP BY GroupID) pd ON pd.GroupID=pm.GroupID'
IF @CategoryID IN(11,12,13,14)
	SET @SQL = @SQL + ' LEFT JOIN Ammo_Table at ON at.UPC=pm.Barcode'
IF @CategoryID IN(15)
	SET @SQL = @SQL + ' LEFT JOIN Shotshell_Table at ON at.UPC=pm.Barcode'
IF @CategoryID IN(219)
	SET @SQL = @SQL + ' LEFT JOIN bullets_from_lg at ON at.vendorsku=pm.vendorsku'
SET @SQL= @SQL + ' WHERE				1=1'

IF @CategoryID is not null
BEGIN
	IF @IsCategoryParent = 1
	SET @SQL=@SQL+' AND (SELECT ParentID FROM Category c WHERE c.ID=pm.CategoryID)=' + cast(@CategoryID AS varchar)
	ELSE
	SET @SQL=@SQL+' AND pm.CategoryID=' + cast(@CategoryID AS varchar)
END
IF @MinPrice is not null
SET @SQL=@SQL+' AND pd.Price>=' + cast(@MinPrice AS varchar)
IF @MaxPrice is not null
SET @SQL=@SQL+' AND pd.Price<=' + cast(@MaxPrice AS varchar)
IF @BrandID is not null
SET @SQL=@SQL+' AND pm.mfgID>=' + cast(@BrandID AS varchar)
IF @InStockOnly=1
SET @SQL=@SQL+' AND pd.Price IS NOT NULL'
IF @NewOnly=1
SET @SQL=@SQL+' AND DATEDIFF(d,pm.recird_date,getdate())<=30'
IF @KeywordString <>'""'
SET @SQL=@SQL+' AND CONTAINS(pm.description, ''' + @KeywordString + ''')'

--Insert the correct columns depending on the category
IF @CategoryID IN(11,12,13,14)
	INSERT INTO @Resultset (ProductID, GroupID, CategoryID, MfgID, Caliber, BulletType, Rounds, Weight) exec (@sql)
IF @CategoryID = 15
	INSERT INTO @Resultset (ProductID, GroupID, CategoryID, MfgID, Gauge, Length, ShotSize, ShotWeight, ShotType) exec (@sql)
IF @CategoryID = 219
	INSERT INTO @Resultset (ProductID, GroupID, CategoryID, MfgID, Caliber) exec (@sql)
IF @CategoryID NOT IN(11,12,13,14,15,219) OR @CategoryID is null
	INSERT INTO @Resultset (ProductID, GroupID, CategoryID, MfgID) exec (@sql)

--Return search results
IF @StartRow>1
SET @SQL='SELECT				ProductID, ProductTitle, ProductDescriptionShort, ImageLocation, Price, MSRP, Discount, PricePerUnit, QuantityTotal, QuantityAtThisPrice
FROM (
SELECT'
ELSE
SET @SQL= 'SELECT'
IF @StartRow=1
SET @SQL=@SQL + ' TOP 15'
SET @SQL=@SQL +' 				 r.ProductID AS ProductID
					,pm.Title AS ProductTitle
					,pm.ShortDesc AS ProductDescriptionShort
					,pm.Thumbnail AS ImageLocation
					,pd.Price
					,pm.MSRP AS MSRP
					,CASE WHEN MSRP>0 THEN (MSRP-pd.price)/MSRP ELSE null END AS Discount
					,CASE WHEN pm.UnitQty>0 THEN pd.price/pm.UnitQty ELSE null END AS PricePerUnit
					,pd.QuantityTotal
					,pd.QuantityAtThisPrice'
IF @StartRow>1
BEGIN
SET @SQL=@SQL + '					,ROW_NUMBER() OVER (ORDER BY '
SET @SQL=@SQL + CASE @SortBy	WHEN 'Newest' THEN 'pm.Recird_Date DESC' 
								WHEN 'Price Low to High' THEN 'pd.Price'
								WHEN 'Price High To Low' THEN 'pd.Price DESC'
								WHEN 'Popularity' THEN '(SELECT SoldQty FROM TopSellingProduct ts WHERE ts.GroupID=pm.GroupID) DESC'
								WHEN 'Price per Unit' THEN 'COALESCE(CASE WHEN UnitQty>0 THEN pd.price/UnitQty ELSE null END,999999999)'
								ELSE ''
					END
SET @SQL=@SQL + ') AS RankNum'
END

SET @SQL=@SQL + ' FROM				@Resultset r
 INNER JOIN			ProductMaster_T pm ON pm.ID=r.ProductID
 INNER JOIN			(SELECT GroupID, MIN(WHPrice) AS Price, SUM(Qty) AS QuantityTotal, SUM(CASE WHEN PriceRank=1 THEN Qty ELSE 0 END) AS QuantityAtThisPrice FROM (SELECT GroupID, WHPrice, SUM(Qty) AS Qty, ROW_NUMBER() OVER (PARTITION BY GroupID ORDER BY WHPrice) AS PriceRank FROM ProductDetail pd  WHERE Active=1 AND Qty>0 GROUP BY GroupID, WHPrice) sub GROUP BY sub.GroupID) pd ON pd.GroupID=r.GroupID'

IF @StartRow=0
SET @SQL=@SQL + CASE @SortBy	WHEN 'Newest' THEN ' ORDER BY pm.Recird_Date DESC' 
								WHEN 'Price Low to High' THEN ' ORDER BY pd.Price'
								WHEN 'Price High To Low' THEN ' ORDER BY pd.Price DESC'
								WHEN 'Popularity' THEN ' ORDER BY (SELECT SoldQty FROM TopSellingProduct ts WHERE ts.GroupID=pm.GroupID) DESC'
								WHEN 'Price per Unit' THEN ' ORDER BY COALESCE(CASE WHEN UnitQty>0 THEN pd.price/UnitQty ELSE null END,999999999)'
								ELSE ''
					END

IF @StartRow>1
SET @SQL=@SQL + ') sub
WHERE				RankNum BETWEEN ' + CAST(@StartRow AS varchar) + ' AND ' + CAST(@EndRow AS varchar) +
' ORDER BY			RankNum'

exec sp_executesql @SQL,N'@Resultset MyTable READONLY',@Resultset=@Resultset

--Return the number of results
SELECT COUNT(*) AS ResultCount FROM @Resultset

--Return matching categories
SELECT				 CASE WHEN @CategoryID is not null THEN r.CategoryID ELSE null END AS CategoryID
					,MAX(CASE WHEN @CategoryID is not null THEN c.Name ELSE null END) AS CategoryName
					,MAX(CASE WHEN c.ParentID=1 THEN c.ID ELSE c.ParentID END) AS ParentCategoryID
					,MAX(CASE WHEN c.ParentID=1 THEN c.name ELSE pc.Name END) AS ParentCategoryName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
 INNER JOIN			Category c ON c.ID=r.CategoryID
 LEFT JOIN			Category pc ON pc.ID=c.ParentID-- AND c.ParentID<>1
GROUP BY			CASE WHEN @CategoryID is not null THEN r.CategoryID ELSE null END
					,CASE WHEN c.ParentID=1 THEN c.name ELSE pc.Name END

ORDER BY			CategoryName

--Return matching brands
SELECT				 r.mfgID AS BrandID
					,MAX(m.Name) AS BrandName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
 INNER JOIN			Manufacturer m ON m.ID=r.MfgID
GROUP BY			 r.mfgID
ORDER BY			BrandName

--Return matching category specific filters

IF @CategoryID IN(11,12,13,14)
BEGIN

SELECT				 'Caliber' AS FilterName
					,r.Caliber AS AttributeID
					,COALESCE(r.Caliber, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.Caliber
UNION ALL
SELECT				 'Bullet Type' AS FilterName
					,r.BulletType AS AttributeID
					,COALESCE(r.BulletType, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.BulletType
UNION ALL
SELECT				 'Rounds' AS FilterName
					,CAST(r.Rounds AS varchar(80)) AS AttributeID
					,COALESCE(CAST(r.Rounds AS varchar(80)), ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.Rounds
UNION ALL
SELECT				 'Weight' AS FilterName
					,r.Weight AS AttributeID
					,COALESCE(r.Weight, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.Weight
ORDER BY			FilterName, AttributeName
END

IF @CategoryID IN(15)
BEGIN

SELECT				 'Gauge' AS FilterName
					,r.Gauge AS AttributeID
					,COALESCE(r.Gauge, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.Gauge
UNION ALL
SELECT				 'Length' AS FilterName
					,r.Length AS AttributeID
					,COALESCE(r.Length, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.Length
UNION ALL
SELECT				 'Shot Size' AS FilterName
					,r.ShotSize AS AttributeID
					,COALESCE(r.ShotSize, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.ShotSize
UNION ALL
SELECT				 'Shot Weight' AS FilterName
					,r.ShotWeight AS AttributeID
					,COALESCE(r.ShotWeight, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.ShotWeight
UNION ALL
SELECT				 'Shot Type' AS FilterName
					,r.ShotType AS AttributeID
					,COALESCE(r.ShotType, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.ShotType
ORDER BY			FilterName, AttributeName
END

IF @CategoryID IN(11,12,13,14)
BEGIN

SELECT				 'Caliber' AS FilterName
					,r.Caliber AS AttributeID
					,COALESCE(r.Caliber, ' Unknown') AS AttributeName
					,COUNT(*) AS ResultCount
FROM				@Resultset r
GROUP BY			r.Caliber
ORDER BY			FilterName, AttributeName
END

IF @CategoryID NOT IN(11,12,13,14,15, 219)
BEGIN
SELECT				 '' AS FilterName
					,'' AS AttributeID
					,'' AS AttributeName
					,0 AS ResultCount
WHERE 1<>1
END

--end try
--begin catch
--	select 'Unknown error'
--end catch


