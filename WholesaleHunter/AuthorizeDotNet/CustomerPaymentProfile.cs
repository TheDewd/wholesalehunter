﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{
    class CustomerPaymentProfile { 
        public int ResultCode { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string LastFour { get; set; }
        public string ExpirationDate { get; set; }
        public List<Message> Messages { get; set; }
        public List<string> SubscriptionIds { get; set; }

        public CustomerPaymentProfile()
        {
            this.Messages = new List<Message>();
            this.SubscriptionIds = new List<string>();
        }
    }
}
