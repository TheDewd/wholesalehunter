﻿using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;

namespace WholesaleHunter.AuthorizeDotNet
{
    class ChargeCreditCard
    {
        //creditCardType
        public string CardNumber { get; set; } //"4111111111111111",
        public string ExpirationDate { get; set; } //mmyy "0718",
        public string CCV { get; set; } // "123"

        public TransactionResult transactionResult { get; set; }
        public CustomerProfileResult profileResult { get; set; }

        public ChargeCreditCard()
        {
            transactionResult = new TransactionResult();
        }

        public void ProcessCharge(ChargeInformation chargeInfo)
        {
            this.CardNumber = chargeInfo.CardNumber;
            this.ExpirationDate = chargeInfo.ExpirationDate;
            this.CCV = chargeInfo.CCV;

            if (ConfigurationManager.AppSettings["AuthorizeNetUseSandbox"].ToLower() == "true")
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ConfigurationManager.AppSettings["AuthorizeNetAPILogin"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ConfigurationManager.AppSettings["AuthorizeNetTransactionKey"],
            };

            var creditCard = new creditCardType
            {
                cardNumber = this.CardNumber,
                expirationDate = this.ExpirationDate,
                cardCode = this.CCV
            };

            var billingAddress = new customerAddressType
            {
                firstName = chargeInfo.BillTo.FirstName,
                lastName = chargeInfo.BillTo.LastName,
                address = chargeInfo.BillTo.Address,
                city = chargeInfo.BillTo.City,
                state = chargeInfo.BillTo.State,
                zip = chargeInfo.BillTo.Zip,
                country = chargeInfo.BillTo.Country,
                email = chargeInfo.BillTo.Email,
                phoneNumber = chargeInfo.BillTo.PhoneNumber,
                faxNumber = chargeInfo.BillTo.FaxNumber,
                company = chargeInfo.BillTo.Company
            };

            var shippingAddress = new customerAddressType
            {
                firstName = chargeInfo.ShipTo.FirstName,
                lastName = chargeInfo.ShipTo.LastName,
                address = chargeInfo.ShipTo.Address,
                city = chargeInfo.ShipTo.City,
                state = chargeInfo.ShipTo.State,
                zip = chargeInfo.ShipTo.Zip,
                country = chargeInfo.ShipTo.Country,
                email = chargeInfo.ShipTo.Email,
                phoneNumber = chargeInfo.ShipTo.PhoneNumber,
                faxNumber = chargeInfo.ShipTo.FaxNumber,
                company = chargeInfo.ShipTo.Company
            };

            var orderInfo = new orderType
            {
                invoiceNumber = chargeInfo.OrderID.ToString(),
                description = chargeInfo.OrderDescription
            };

            var customerInfo = new customerDataType
            {
                type = AuthorizeNet.Api.Contracts.V1.customerTypeEnum.individual,
                id = chargeInfo.CustomerID.ToString(),
                email = chargeInfo.BillTo.Email,
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // charge the card

                amount = chargeInfo.TransactionAmount,
                payment = paymentType,
                billTo = billingAddress,
                shipTo = shippingAddress,
                order = orderInfo,
                customer = customerInfo
                //lineItems = lineItems
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            //validate
            if (response != null)
            {
                transactionResult.TransactionID = response.transactionResponse.transId;
                transactionResult.ResponseCode = response.transactionResponse.responseCode;
                transactionResult.AuthCode = response.transactionResponse.authCode;
                transactionResult.TransactionResultCode = (int)response.messages.resultCode;
                transactionResult.AVSResultCode = response.transactionResponse.avsResultCode;
                transactionResult.CAVVResultCode = response.transactionResponse.cavvResultCode;
                transactionResult.CVVResultCode = response.transactionResponse.cvvResultCode;

                if(response.transactionResponse.errors==null)
                {
                    transactionResult.ErrorCode = "1";
                }
                else
                { 
                    foreach(transactionResponseError err in response.transactionResponse.errors)
                    {
                        transactionResult.ErrorCode = err.errorCode;
                        break;
                    }
                }

                if (response.transactionResponse.messages != null)
                {
                    for (int cnt = 0; cnt < response.transactionResponse.messages.Length; cnt++)
                    {
                        TransactionMessage msg = new TransactionMessage();
                        msg.code = response.transactionResponse.messages[cnt].code;
                        msg.description = response.transactionResponse.messages[cnt].description;
                        transactionResult.Messages.Add(msg);
                    }
                }

                if (response.transactionResponse.errors != null)
                {
                    for (int cnt = 0; cnt < response.transactionResponse.errors.Length; cnt++)
                    {
                        TransactionError err = new TransactionError();
                        err.errorCode = response.transactionResponse.errors[cnt].errorCode;
                        err.errorText = response.transactionResponse.errors[cnt].errorText;
                        transactionResult.Errors.Add(err);
                    }
                }

                if(response.messages.resultCode == messageTypeEnum.Ok)
                {
                    // the transaction was succesful
                    if(chargeInfo.CreateProfile)
                    {
                        CustomerProfileBase profileBase = new CustomerProfileBase();
                        profileBase.CustomerID = chargeInfo.CustomerID;
                        profileBase.TransactionID = transactionResult.TransactionID;
                        profileBase.Email = chargeInfo.BillTo.Email;
                        profileBase.Description = "Profile for customer id " + chargeInfo.CustomerID.ToString();

                        CreateCustomerProfileFromTransaction createProfile = new CreateCustomerProfileFromTransaction();
                        this.profileResult = createProfile.createProfile(profileBase);
                    }
                }
            }
        }
    }
}