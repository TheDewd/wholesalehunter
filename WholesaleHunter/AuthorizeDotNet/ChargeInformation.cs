﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{ 
    class ChargeInformation
    {
        public int CustomerID { get; set; }
        public decimal TransactionAmount { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string CCV { get; set; }
        public int OrderID { get; set; }
        public string OrderDescription { get; set; }
        public CustomerAddress BillTo { get; set; }
        public CustomerAddress ShipTo { get; set; }


        public bool CreateProfile { get; set; }

        public ChargeInformation()
        {

        }
    }
}
