﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WholesaleHunter.AuthorizeDotNet
{
    class CustomerProfileBase { 
        public string TransactionID { get; set; }
        public int CustomerID { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
    }
}
