﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;

namespace WholesaleHunter.AuthorizeDotNet
{
    class GetCustomerPaymentProfile
    {
        public GetCustomerPaymentProfile()
        { }

        public CustomerPaymentProfile GetProfile(string customerProfileId, string customerPaymentProfileId)
        {
            CustomerPaymentProfile profile = new CustomerPaymentProfile();

            if(ConfigurationManager.AppSettings["AuthorizeNetUseSandbox"].ToLower() == "true")
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }
            
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ConfigurationManager.AppSettings["AuthorizeNetAPILogin"],
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ConfigurationManager.AppSettings["AuthorizeNetTransactionKey"],
            };

            var request = new getCustomerPaymentProfileRequest();
            request.customerProfileId = customerProfileId;
            request.customerPaymentProfileId = customerPaymentProfileId;

            // Set this optional property to true to return an unmasked expiration date
            request.unmaskExpirationDateSpecified = true;
            request.unmaskExpirationDate = true;

            // instantiate the controller that will call the service
            var controller = new getCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null)
            {
                profile.ResultCode = (int)response.messages.resultCode;
                for(int cnt=0;cnt<response.messages.message.Length;cnt++)
                {
                    Message msg = new Message();
                    msg.text = response.messages.message[cnt].text;
                    msg.code = response.messages.message[cnt].code;
                    profile.Messages.Add(msg);
                }

                profile.CustomerPaymentProfileId = response.paymentProfile.customerPaymentProfileId;

                if (response.paymentProfile.payment.Item is creditCardMaskedType)
                {
                    profile.LastFour = (response.paymentProfile.payment.Item as creditCardMaskedType).cardNumber;
                    profile.ExpirationDate = (response.paymentProfile.payment.Item as creditCardMaskedType).expirationDate;

                    if (response.paymentProfile.subscriptionIds != null && response.paymentProfile.subscriptionIds.Length > 0)
                    {
                        for (int i = 0; i < response.paymentProfile.subscriptionIds.Length; i++)
                            profile.SubscriptionIds.Add(response.paymentProfile.subscriptionIds[i]);
                    }
                }
            }
            return profile;
        }
    }
}

