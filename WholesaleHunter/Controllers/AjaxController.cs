﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WholesaleHunter.Models;
using Newtonsoft.Json;
using WholesaleHunter.Classes;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Text;
using System.Globalization;

namespace WholesaleHunter.Controllers
{
    [AllowAnonymous]
    public class AjaxController : ApiController
    {
        public AppUserState appuser;
        private string dbname = "WholesaleHunterCom";

        [HttpPost]
        public string GetCartPreview()
        {
            int currentUserID = 0;
            CartPreview prev;
            if (getClaims())
            {
                currentUserID = appuser.UserID;
            }

            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", currentUserID);
                    prev = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault<CartPreview>();
                    CartPreviewDisplay prevDisplay = new CartPreviewDisplay(prev);
                    return Newtonsoft.Json.JsonConvert.SerializeObject(prevDisplay);
                }
            }
            catch
            {
                return "Error";
            }
        }

        [HttpPost]
        public string GetItemExactShipping(ItemShipping data)
        {
            ItemShippingCost cost;
            string results;
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@ProductID", data.ProductID);
                    p.Add("@ZipCode", data.ZipCode);
                    cost = db.Query<ItemShippingCost>("WholesaleHunterCom.sItemShipping", p, commandType: CommandType.StoredProcedure).First<ItemShippingCost>();
                }
                results = Newtonsoft.Json.JsonConvert.SerializeObject(cost);
            }
            catch
            {
                results = "0";
            }
            return results;
        }

        [HttpPost]
        public string UpdateCartPaymentMethod(CartPaymentChoice choice)
        {
            int status = 0;
            int currentUserID = 0;
            if (getClaims())
            {
                currentUserID = appuser.UserID;
            }
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    int myPaymentTypeID = 0;
                    Int32.TryParse(choice.PaymentTypeID, out myPaymentTypeID);

                    int? myCustomerPaymentMethodID = null;
                    if (choice.CustomerPaymentMethodID == 0)
                    {
                        myCustomerPaymentMethodID = null;
                    }

                    var p = new DynamicParameters();
                    p.Add("@CustomerID", currentUserID);
                    p.Add("@ShippingMethod", choice.ShippingMethod);
                    p.Add("@PaymentTypeID", myPaymentTypeID);
                    p.Add("@CustomerPaymentMethodID", myCustomerPaymentMethodID);
                    status = db.Query<int>("WholesaleHunterCom.uCartPaymentChoices", p, commandType: CommandType.StoredProcedure).Single<int>();
                }
            }
            catch
            {
                status = 0;
            }
            return status.ToString();
        }

        [HttpPost]
        public string CheckCartInventory(ShippingInfo info)
        {
            int status = 0;
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", info.CustomerID);
                    status = db.Query<int>("WholesaleHunterCom.diuVerifyCartInventory", p, commandType: CommandType.StoredProcedure).Single<int>();
                }
            }
            catch
            {
                status = 0;
            }
            return status.ToString();
        }

        [HttpPost]
        public string GetExactShipping(ShippingInfo info)
        {
            CartShipping amounts = null;
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("@CustomerID", info.CustomerID);
                p.Add("@ZipCode", info.ZipCode);
                amounts = db.Query<CartShipping>("WholesaleHunterCom.sShoppingCartExactShipping", p, commandType: CommandType.StoredProcedure).Single<CartShipping>();
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(amounts);
        }


        [HttpPost]
        public string UpdateCart(CartItemUpdate item)
        {
            Cart myCart = new Cart(item.CustomerID);
            int status = myCart.UpdateCart(item);
            return status.ToString();
        }



        [HttpPost]
        public string AddToCart(CartItem item)
        {
            CartPreview prev = null;
            CartPreviewDisplay prevDisplay = null;
            string errormessage = "";
            Cart myCart = new Cart(item.CustomerID);
            int status = myCart.AddItemToCart(item);
            if(status<0)
            {
                return myCart.ErrorMessage;
            }

            //see if this is a new customer, if so log them in
            if (myCart.IsNewCustomer)
            {
                //the value in result will be the new customer id
                item.CustomerID = myCart.NewCustomerID;
                //we need to log the user in with the new customer id
                //create a Logon object and send to Action/LogOn
                AppUserState appUserState = new AppUserState();
                appUserState.Email = "";
                appUserState.UserID = item.CustomerID;

                IdentitySignin(appUserState, "providerKey");

            }

            //now we need to do a cart preview to get the count and total of items in the cart
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", item.CustomerID);
                    prev = db.Query<CartPreview>("WholesaleHunterCom.sCartPreview", p, commandType: CommandType.StoredProcedure).FirstOrDefault<CartPreview>();
                    prevDisplay = new CartPreviewDisplay(prev);
                }
            }
            catch
            {
                errormessage = "Error: getting Cart Preview data.";
            }

            if(errormessage!= string.Empty)
            {
                return errormessage;
            }
            else
            {

            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(prevDisplay);
        }


        [HttpPost]
        public string NotifyOfInventory([FromBody] NotifyInventory data)
        {
            int status = 0;
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@ProductID", data.ProductID);
                    p.Add("@Email", data.Email);
                    p.Add("@CustomerID", data.CustomerID);
                    status = db.Query<int>("WholesaleHunterCom.iNotifyOfInventory", p, commandType: CommandType.StoredProcedure).FirstOrDefault<int>();
                }
            }
            catch
            {
                status = 0;
            }
            return status.ToString();
        }

        [HttpPost]
        public string GetPricePoints([FromBody] ProductID prod)
        {
            StringBuilder output = new StringBuilder();
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
                    nfi = (NumberFormatInfo)nfi.Clone();
                    nfi.CurrencySymbol = "$";

                    var p = new DynamicParameters();
                    p.Add("@ProductID", prod.ID);
                    var data = db.Query<PricePoints>("WholesaleHunterCom.sProductPrices", p, commandType: CommandType.StoredProcedure);
                    List<PricePoints> listPrices = data.ToList<PricePoints>();

                    output.Append("<table><tr class=\"prices\"><th>Qty</th><th>Price</th></tr>");
                    foreach (PricePoints price in listPrices)
                    {
                        output.Append("<tr class=\"prices\">");
                        output.Append("<td>");
                        output.Append(price.Qty);
                        output.Append("</td><td>");
                        output.Append(String.Format(nfi, "{0:C}", price.WHPrice));
                        output.Append("</td></tr>");
                    }
                    output.Append("</table>");
                }
            }
            catch
            {
                output = new StringBuilder();
            }
            return output.ToString();
        }

        [HttpPost]
        public string CreateAccount([FromBody] NewAccount data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@Username", data.Email);
                    p.Add("@Password", data.Password);
                    p.Add("@FirstName", data.FirstName);
                    p.Add("@LastName", data.LastName);
                    p.Add("@Address1", data.Address1);
                    p.Add("@Address2", data.Address2);
                    p.Add("@City", data.City);
                    p.Add("@State", data.State);
                    p.Add("@ZIPCode", data.Zip);
                    p.Add("@Phone", data.Phone);
                    int userid = db.Query<int>("WholesaleHunterCom.iLoginCreate", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (userid == -1)
                    {
                        //the email address is already in use
                        status = "inuse";
                    }
                    else
                    {
                        //create a Logon object and send to Action/LogOn
                        AppUserState appUserState = new AppUserState();
                        appUserState.Email = data.Email;
                        appUserState.UserID = userid;

                        IdentitySignin(appUserState, "providerKey");

                        return "ok";
                    }

                }
            }
            catch
            {
                status = "err";
            }
            return status;

        }


        [HttpPost]
        public string ForgotPW([FromBody] Logon data)
        {
            int err = 0;
            string forgottenpw = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@Username", data.username);
                    PasswordEmail pword = db.Query<PasswordEmail>("WholesaleHunterCom.sPasswordEmail", p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    forgottenpw = pword.Password;
                }

                MailAddress To = new MailAddress(data.username);
                MailAddress From = new MailAddress(ConfigurationManager.AppSettings["LostPasswordMailFrom"]);
                string Subject = ConfigurationManager.AppSettings["LostPasswordMailSubject"];

                StringBuilder body = new StringBuilder();
                body.Append(ConfigurationManager.AppSettings["LostPasswordMessageText"]);
                body.Append(forgottenpw);

                SMTPEmail mailer = new SMTPEmail();
                mailer.Compose(Subject, To, From, body.ToString());
                mailer.Send();
            }
            catch
            {
                err = 1;
            }

            if (err == 1)
            {
                return "failed";
            }
            else
            {
                return "ok";
            }
        }


        [HttpPost]
        public string LogOn([FromBody] Logon data)
        {
            Login user;
            int results = 0;
            int currentUserID = 0;
            if (getClaims())
            {
                currentUserID = appuser.UserID;
            }

            //At this point, check the db and see if the user is present
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {

                var p = new DynamicParameters();
                p.Add("@Username", data.username);
                p.Add("@Password", data.password);

                user = db.Query<Login>("[WholesaleHunterCom].[sLoginVerify]", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (user == null)
                {
                    return "failed";
                }

                if (user.CustomerID == 0)
                {
                    return "failed";
                }
            }

            AppUserState appUserState = new AppUserState();
            appUserState.Email = data.username;
            appUserState.UserID = user.CustomerID;

            IdentitySignin(appUserState, "providerKey");

            //now we need to transfer cart contents from currentUserID to appUserState.UserID
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
            {

                var p = new DynamicParameters();
                p.Add("@OldCustomerID", currentUserID);
                p.Add("@NewCustomerID", appUserState.UserID);

                results = db.Query<int>("[WholesaleHunterCom].[uCartOwnership]", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (results == 0)
                {
                    //an error occured switching cart contents
                    return "failed";
                }

                //now check the cart contents
                p = new DynamicParameters();
                p.Add("@CustomerID", appUserState.UserID);
                var check = db.QueryMultiple("WholesaleHunterCom.sShoppingCart", p, commandType: CommandType.StoredProcedure);

                List<CartListItem> CartItems = check.Read<CartListItem>().ToList();
                CartShipping Shipping = check.Read<CartShipping>().Single();
                List<CartSuggestedProducts> SuggestedProducts = check.Read<CartSuggestedProducts>().ToList();
                decimal ShippingAmount;
                if (!Decimal.TryParse(Shipping.ShippingCost, out ShippingAmount))
                {
                    ShippingAmount = 0;
                }

                if (ShippingAmount == -2)
                {
                    return ("carterror1");
                }
            }
            return "ok";
        }


        [HttpPost]
        public string FetchProducts([FromBody] FetchProductParameters ParamsIn)
        {
            string results;
            string storedProcedureName = "";
            var p = new DynamicParameters();
            p.Add("@StartRow", ParamsIn.StartRow);
            p.Add("@EndRow", ParamsIn.EndRow);

            switch (ParamsIn.SliderID)
            {
                case 1:
                    if (ParamsIn.EndRow == 5)
                    {
                        //doesn't require any parameters
                        p = new DynamicParameters();
                        storedProcedureName = "WholesaleHunterCom.sTopSellingInitial";
                    }
                    else
                    {
                        storedProcedureName = "WholesaleHunterCom.sTopSelling";
                    }
                    break;
                case 2:
                    storedProcedureName = "WholesaleHunterCom.sNewProducts";
                    break;
                case 3:
                    storedProcedureName = "WholesaleHunterCom.sClearance";
                    break;
                case 4:
                    storedProcedureName = "WholesaleHunterCom.sRecentlyViewed";
                    p.Add("@CustomerID", ParamsIn.CustomerID);
                    break;
                case 5:
                case 6:
                case 7:
                    storedProcedureName = "WholesaleHunterCom.sTopCategory";
                    p.Add("@CategoryID", ParamsIn.CategoryID);
                    break;

            }
            try
            {

                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var raw = db.Query<Product>(storedProcedureName, p, commandType: CommandType.StoredProcedure);

                    List<Product> products = raw.ToList<Product>();
                    results = JsonConvert.SerializeObject(products);
                }
            }
            catch
            {
                results = "0";
            }
            return results;
        }


        #region SignIn and Signout

        /// <summary>
        /// Helper method that adds the Identity cookie to the request output
        /// headers. Assigns the userState to the claims for holding user
        /// data without having to reload the data from disk on each request.
        /// 
        /// AppUserState is read in as part of the baseController class.
        /// </summary>
        /// <param name="appUserState"></param>
        /// <param name="providerKey"></param>
        /// <param name="isPersistent"></param>
        public void IdentitySignin(AppUserState appUserState, string providerKey = null)
        {
            var claims = new List<Claim>();

            // create *required* claims
            claims.Add(new Claim(ClaimTypes.Email, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, appUserState.Email));
            claims.Add(new Claim(ClaimTypes.Name, appUserState.Email));
            // serialized AppUserState object
            claims.Add(new Claim("userState", appUserState.ToString()));

            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            var ctx = Request.GetOwinContext();
            var AuthenticationManager = ctx.Authentication;

            // add to user here!
            int ExpireDays = Int32.Parse(ConfigurationManager.AppSettings["ExpireAuthenticationCookieInDays"]);
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                IsPersistent = true,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            };

            AuthenticationManager.SignIn(authProperties, identity);
        }

        public void IdentitySignout()
        {
            var ctx = Request.GetOwinContext();
            var AuthenticationManager = ctx.Authentication;
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
                DefaultAuthenticationTypes.ExternalCookie);
        }

        #endregion

        //The following methods are for the the MyAddress section of MyAccount

        [HttpPost]
        public string UpdateAddress([FromBody] MyAddress data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerAddressID", data.CustomerAddressID);
                    p.Add("@AddressName", data.AddressName);
                    p.Add("@Address1", data.Address1);
                    p.Add("@Address2", data.Address2);
                    p.Add("@City", data.City);
                    p.Add("@State", data.State);
                    p.Add("@ZIPCode", data.ZIPCode);

                    int result = db.Query<int>("WholesaleHunterCom.uMyAddress", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }
                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        [HttpPost]
        public string DeleteAddress([FromBody] MyAddress data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerAddressID", data.CustomerAddressID);

                    int result = db.Query<int>("WholesaleHunterCom.dMyAddress", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }
                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        [HttpPost]
        public string MakePrimary([FromBody] MyAddress data)
        {
            int currentUserID=0;
            if (getClaims())
            {
                currentUserID = appuser.UserID;
            }
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", currentUserID);
                    p.Add("@CustomerAddressID", data.CustomerAddressID);
                    int result = db.Query<int>("WholesaleHunterCom.uMyAddressMakePrimary", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }
                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        [HttpPost]
        public string AddNewAddress([FromBody] MyAddress data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", data.CustomerID);
                    p.Add("@AddressName", data.AddressName);
                    p.Add("@Address1", data.Address1);
                    p.Add("@Address2", data.Address2);
                    p.Add("@City", data.City);
                    p.Add("@State", data.State);
                    p.Add("@ZIPCode", data.ZIPCode);

                    int result = db.Query<int>("WholesaleHunterCom.iMyAddress", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }
                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        //The following methods are for the MyPaymentMethods section of My account

        [HttpPost]
        public string DeletePaymentMethod([FromBody] MyPaymentMethod data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerPaymentMethodID", data.CustomerPaymentMethodID);

                    int result = db.Query<int>("WholesaleHunterCom.dMyPaymentMethod", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }
                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        [HttpPost]
        public string MakePrimaryPaymentMethod([FromBody] MyPaymentMethod data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", data.CustomerID);
                    p.Add("@CustomerPaymentMethodID", data.CustomerPaymentMethodID);
                    int result = db.Query<int>("WholesaleHunterCom.uMyPaymentMethodMakePrimary", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }
                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        [HttpPost]
        public string AddNewPaymentMethod([FromBody] MyPaymentMethod data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var expirationDate = data.ExpirationDate.ToShortDateString();
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", data.CustomerID);
                    p.Add("@LastFour", data.LastFour);
                    p.Add("@AuthorizeNETToken", data.AuthorizeNETToken);
                    p.Add("@ExpirationDate", expirationDate);

                    int result = db.Query<int>("WholesaleHunterCom.iMyPaymentMethod", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }

                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        [HttpPost]
        public string UpdateContactPreferences([FromBody] Customer data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", data.CustomerID);
                    p.Add("@Phone", data.Phone);
                    p.Add("@Email", data.EMail);
                    p.Add("@GetNewsletter", data.GetNewsletter);

                    int result = db.Query<int>("WholesaleHunterCom.uMyContactPreferences", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else
                    {
                        status = "err";
                    }

                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        [HttpPost]
        public string UpdatePassword([FromBody] MyPassword data)
        {
            string status = "";
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings[dbname].ConnectionString))
                {
                    var p = new DynamicParameters();
                    p.Add("@CustomerID", data.CustomerID);
                    p.Add("@CurrentPassword", data.Password1);
                    p.Add("@NewPassword", data.Password2);

                    int result = db.Query<int>("WholesaleHunterCom.uMyPassword", p, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (result == 1)
                    {
                        status = "ok";
                    }
                    else if (result == -1)
                    {
                        status = "wrongpassword";
                    }
                    else
                    {
                        status = "err";
                    }

                }
            }
            catch
            {
                status = "err";
            }
            return status;
        }

        private bool getClaims()
        {
            bool status;
            try
            {
                this.appuser = new AppUserState();
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims;
                foreach (Claim claim in claims)
                {
                    if (claim.Type == "userState")
                    {
                        if (!appuser.FromString(claim.Value))
                        {
                            return false;
                        }
                    }
                }
                status = true;
            }
            catch
            {
                status = false;
            }
            return status;
        }
    }
}