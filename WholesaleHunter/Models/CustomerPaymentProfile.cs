﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class PaymentProfile {
        public string CustomerProfileID { get; set; }
        public string CustomerPaymentProfileID { get; set; }
    }
}