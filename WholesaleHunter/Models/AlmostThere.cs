﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class AlmostThere {
        public int PaymentMethod { get; set; }
        public int SelectCard { get; set; }
        public string SelectedCardDescription { get; set; }
        public string SelectedCardPaymentMethodId { get; set; }
        public string CardNumber { get; set; }
        public string ExpireMonth { get; set; }
        public string ExpireYear { get; set; }
        public string CVV { get; set; }
        public string CreditCardZIP { get; set; }
        public int RememberCard { get; set; }
        public int MakeDefault { get; set; }
        public string ShippingMethod { get; set; }
        public string ShippingMethodName { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal Hazmat { get; set; }
        public int ShippingInsurance { get; set; }
        public decimal ShippingInsuranceCost { get; set; }
        public decimal Tax { get; set; }
        public decimal OrderTotal { get; set; }
        public string OrderDescription { get; set; }

        //Shipping address
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingZip { get; set; }

        //For credit card charges
        public string Address { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string BillingName { get; set; }
        public string BillingPhone { get; set; }
    }
}