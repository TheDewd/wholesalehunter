﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class Manufacturer
    {
        public int ID { get; set; }
        public string ManufacturerName { get; set; }
        public string Abbv { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CityStateZip { get; set; }
        public string BannerLink { get; set; }
        public string Logo { get; set; }
        public int StateID { get; set; }
        public string UseMap { get; set; }
        public string SEOMetaDesc { get; set; }
        public string SEOHTMLDesc { get; set; }
        public string SEOKeywords { get; set; }
        public int MfgID { get; set; }

    }
}