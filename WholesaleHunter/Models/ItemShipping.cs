﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class ItemShipping
    {
        public int ProductID { get; set; }
        public string ZipCode { get; set; }
    }

}