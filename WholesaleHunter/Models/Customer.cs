﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }

        public string Phone { get; set; }
        
        public string EMail { get; set; }

        public bool GetNewsletter { get; set; }

    }
}