﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class MonthlySalesSummary
    {
        public string TrafficeSourceTargetSite { get; set; }
        public string Date { get; set; }
        public decimal MonthTotal { get; set; }
        public decimal TP { get; set; }
    }
}