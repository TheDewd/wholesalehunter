﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartItem
    {
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string SourceID { get; set; }
    }
}