﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartMyInfoSubmit
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string VerifyEmail { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIPCode { get; set; }
        public string Password { get; set; }
        public string VerifyPassword { get; set; }
        public int FFLRequired { get; set; }
        public bool IsRegistered {get;set;}
        public bool IsGuest { get; set; }
        public bool IsNewUser { get; set; }

    }
}