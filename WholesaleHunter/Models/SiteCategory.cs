﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SiteCategory
    {
        public string CategoryName { get; set; }

        public string CategoryDescription { get; set; }

        public int CategoryID { get; set; }

        public string SubCategoryName { get; set; }

        public string SubCategoryDescription { get; set; }

        public int SubCategoryID { get; set; }

        public int ParentID { get; set; }


    }
}