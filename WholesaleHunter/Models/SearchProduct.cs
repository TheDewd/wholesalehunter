﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SearchProduct
    {
        public int ProductID { get; set; }
        public string ProductTitle { get; set; }
        public string ProductDescriptionShort { get; set; }
        public string ImageLocation { get; set; }
        public decimal Price { get; set; }
        public decimal MSRP { get; set; }
        public double Discount { get; set; }
        public double PricePerUnit { get; set; }
        public int QuantityTotal { get; set; }
        public int QuantityAtThisPrice { get; set; }
        public int FreeShipping { get; set; }
        public int FlatShipping { get; set; }
        public string keywords { get; set; }
        public string MfgNo { get; set; }
        public string SKU { get; set; }
        public string mfgName { get; set; }
        public string ParentCategoryName { get; set; }
        public string CategoryName { get; set; }
    }
}