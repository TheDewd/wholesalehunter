﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class ChargeStatus
    {
        public int Status { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorText{ get; set; }

    }
}