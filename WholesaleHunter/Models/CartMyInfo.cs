﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartMyInfo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZIPCode { get; set; }
        public string FFLID { get; set; }
        public string FFLContactName { get; set; }
        public string FFLEmail { get; set; }
        public string FFLPhone { get; set; }
        public string FFLAddress1 { get; set; }
        public string FFLCity { get; set; }
        public string FFLState { get; set; }
        public string FFLZIPCode { get; set; }
        public string FFLName { get; set; }
        public int FFLRequired { get; set; }
    }
}