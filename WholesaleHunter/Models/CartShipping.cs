﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartShipping
    {
        public string ShippingCost { get; set; }
        public decimal? Tax { get; set; }
        public decimal Hazmat { get; set; }
    }
}