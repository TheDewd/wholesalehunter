﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class NotifyInventory
    {
        public int ProductID { get; set; }
        public string Email { get; set; }
        public int CustomerID { get; set; }
    }
}