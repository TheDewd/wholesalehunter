﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class ItemShippingCost
    {
        public string ShippingCost { get; set; }
        public decimal Hazmat { get; set; }
    }
}