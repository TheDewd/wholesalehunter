﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class FilterSelected
    {
        public string FilterName { get; set; }
        public string FilterValue { get; set; }
        public string FilterText { get; set; }
    }
}