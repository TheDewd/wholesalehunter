﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartShippingMethod
    {
        public string ShippingMethod { get; set; }
        public string ShippingCost { get; set; }
        public string ShippingMethodName { get; set; }
        public string FromAddress { get; set; }
        public string FromCity { get; set; }
        public string FromState { get; set; }
        public string FromZIP { get; set; }
        public int Shipments { get; set; }
        public string ToAddress { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ToZIP { get; set; }
        public double Weight { get; set; }
    }
}