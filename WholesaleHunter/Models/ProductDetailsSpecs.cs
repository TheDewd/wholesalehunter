﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class ProductDetailsSpecs
    {
        public string SpecName { get; set; }
        public string SpecValue { get; set; }
    }
}