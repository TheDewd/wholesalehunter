﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class SearchCategory
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int ParentCategoryID { get; set; }
        public string ParentCategoryName { get; set; }
        public int ResultCount { get; set; }
        public string CssClass { get; set; }
    }
}