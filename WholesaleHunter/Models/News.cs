﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class News
    {
        public string Headline { get; set; }
        public string LinkTarget { get; set; }
        public string ImageLocation { get; set; }
        public string StoryTextPreview { get; set; }
        public DateTime UpdateDate { get; set; }

    }
}