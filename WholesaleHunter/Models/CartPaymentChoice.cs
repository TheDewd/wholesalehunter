﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartPaymentChoice {
        public string ShippingMethod { get; set; }
        public string PaymentTypeID { get; set; }
        public int CustomerPaymentMethodID { get; set; }
    }
}