﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class CartPaymentMethod {
        public int CustomerPaymentMethodID { get; set; }
        public string CardTitle { get; set; }
        public int IsPrimary { get; set; }
        public string BillAddr1 { get; set; }
        public string BillCity { get; set; }
        public string BillState { get; set; }
        public string BillZIP { get; set; }
        public string BillName { get; set; }
        public string BillPhone { get; set; }
    }
}