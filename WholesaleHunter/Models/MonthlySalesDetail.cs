﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Models
{
    public class MonthlySalesDetail
    {
        public string TrafficeSourceTargetSite { get; set; }
        public string Date { get; set; }
        public int ID { get; set; }
        public DateTime TimeStamp { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TP { get; set; }
    }
}