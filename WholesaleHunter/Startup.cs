﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Div1ASalary.Startup))]
namespace Div1ASalary
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
