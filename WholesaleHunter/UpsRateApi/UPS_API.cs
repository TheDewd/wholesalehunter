﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace UpsRateApi
{
    public class UPS_API
    {
        private string JsonTemplate;

        public const string UPS_GROUND = "03";
        public const string UPS_2ND_DAY_AIR = "02";
        public const string UPS_SUREPOST = "93";

        public string Username { get; set; }
        public string Password { get; set; }
        public string AccessLicense { get; set; }
        public string CustomerID { get; set; }
        public ShipAddress Shipper { get; set; }
        public ShipAddress ShipTo { get; set; }
        public ShipAddress ShipFrom { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceDescription { get; set; }
        public string Weight { get; set; }
        public string NegotiatedRatesIndicator { get; set; }

        public UPS_API()
        {
            GetJsonTemplate();
            //make sure there are no nulls
            this.Username = string.Empty;
            this.Password = string.Empty;
            this.AccessLicense = string.Empty;
            this.CustomerID = string.Empty;
            this.ServiceCode = string.Empty;
            this.ServiceDescription = string.Empty;
            this.Weight = string.Empty;
            this.NegotiatedRatesIndicator = string.Empty;

            ShipAddress blank = new ShipAddress();
            this.Shipper = blank;
            this.ShipTo = blank;
            this.ShipFrom = blank;
        }

        public RateResponse GetRateResponse()
        {
            GetJsonTemplate();
            PopulateJsonTemplate();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // create a request
            string url = ConfigurationManager.AppSettings["UpsUrl"];
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            request.Method = "POST";

            // turn our request string into a byte stream
            byte[] postBytes = Encoding.UTF8.GetBytes(JsonTemplate);

            // this is important - make sure you specify type this way
            
            request.Accept = "application/json";
            request.ContentLength = postBytes.Length;
            //request.CookieContainer = Cookies;
            //request.UserAgent = currentUserAgent;
            request.Headers.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            request.Headers.Add("Access-Control-Allow-Methods", "POST");
            request.Headers.Add("Access-Control-Allow-Origin", "*");
            Stream requestStream = request.GetRequestStream();

            // now send it
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            // grab te response and print it out to the console along with the status code
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            RateResponse resp;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                resp = new RateResponse(rdr.ReadToEnd());                
            }
            return resp;
        }



        private void PopulateJsonTemplate()
        {
            this.JsonTemplate = this.JsonTemplate.Replace("<username>", this.Username);
            this.JsonTemplate = this.JsonTemplate.Replace("<password>", this.Password);
            this.JsonTemplate = this.JsonTemplate.Replace("<accesslicense>", this.AccessLicense);
            this.JsonTemplate = this.JsonTemplate.Replace("<customerid>", this.CustomerID);

            this.JsonTemplate = this.JsonTemplate.Replace("<shipper.name>", this.Shipper.Name);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipper.id>", this.Shipper.ID);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipper.addressline>", this.Shipper.AddressLine);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipper.city>", this.Shipper.City);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipper.state>", this.Shipper.State);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipper.zip>", this.Shipper.Zip);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipper.country>", this.Shipper.Country);

            this.JsonTemplate = this.JsonTemplate.Replace("<shipto.name>", this.ShipTo.Name);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipto.addressline>", this.ShipTo.AddressLine);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipto.city>", this.ShipTo.City);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipto.state>", this.ShipTo.State);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipto.zip>", this.ShipTo.Zip);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipto.country>", this.ShipTo.Country);

            this.JsonTemplate = this.JsonTemplate.Replace("<shipfrom.name>", this.ShipTo.Name);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipfrom.addressline>", this.ShipTo.AddressLine);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipfrom.city>", this.ShipTo.City);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipfrom.state>", this.ShipTo.State);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipfrom.zip>", this.ShipTo.Zip);
            this.JsonTemplate = this.JsonTemplate.Replace("<shipfrom.country>", this.ShipTo.Country);

            this.JsonTemplate = this.JsonTemplate.Replace("<servicecode>", this.ServiceCode);
            this.JsonTemplate = this.JsonTemplate.Replace("<servicedescription>", this.ServiceDescription);
            this.JsonTemplate = this.JsonTemplate.Replace("<weight>", this.Weight);
            this.JsonTemplate = this.JsonTemplate.Replace("<negotiatedratesindicator>", "this.NegotiatedRatesIndicator");
        }

        private void GetJsonTemplate()
        {
            this.JsonTemplate = @"{
			""UPSSecurity"": {
				""UsernameToken"": {
					""Username"": ""<username>"", 
					""Password"": ""<password>"" 
				}, 
				""ServiceAccessToken"": {
					""AccessLicenseNumber"": ""<accesslicense>"" 
				}
			}, 
			""RateRequest"": {
				""Request"": {
					""RequestOption"": ""Rate"", 
					""TransactionReference"": {
						""CustomerContext"": ""<customerid>""
					}
				}, 
				""Shipment"": {
					""Shipper"": {
						""Name"": ""<shipper.name>"", 
						""ShipperNumber"": ""<shipper.id>"", 
						""Address"": {
							""AddressLine"": [
								""<shipper.addressline>""
							], 
							""City"": ""<shipper.city>"", 
							""StateProvinceCode"": ""<shipper.state>"",
							""PostalCode"": ""<shipper.zip>"",
							""CountryCode"": ""<shipper.country>""
						}
					},
					""ShipTo"": {
						""Name"": ""<shipto.name>"",
						""Address"": {
							""AddressLine"": [
								""<shipto.addressline>"",
							], 
							""City"": ""<shipto.city>"",
						""StateProvinceCode"": ""<shipto.state>"",
						""PostalCode"": ""<shipto.zip>"",
						""CountryCode"": ""<shipto.country>""
					} 
				},
				""ShipFrom"": {
					""Name"": ""<shipfrom.name>"", 
					""Address"": {
						""AddressLine"": [
							""<shipfrom.addressline>"",
							],
							""City"": ""<shipfrom.city>"", 
							""StateProvinceCode"": ""<shipfrom.state>"", 
							""PostalCode"": ""<shipfrom.zip>"", 
							""CountryCode"": ""<shipfrom.country>""
						}
					}, 
					""Service"": {
						""Code"": ""<servicecode>"", 
						""Description"": ""<servicedescription>""
					},
					""Package"": {
						""PackagingType"": {
							""Code"": ""02"", 
							""Description"": ""Rate"" 
						},  
						""PackageWeight"": {
							""UnitOfMeasurement"": {
								""Code"": ""Lbs"", 
								""Description"": ""pounds"" 
							}, 
							""Weight"": ""<weight>"" 
						}
					},
					""ShipmentRatingOptions"": {
						""NegotiatedRatesIndicator"": ""<negotiatedratesindicator>""
					}
				}
			} 
		}";
        }
    }
}