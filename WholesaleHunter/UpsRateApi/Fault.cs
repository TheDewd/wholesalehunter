﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpsRateApi
{
    public class Fault
    {
        public string FaultCode { get; set; }
        public string FaultString { get; set; }
        public string PrimaryErrorSeverity { get; set; }
        public Alert PrimaryErrorCode { get; set; }
        
    }
}
