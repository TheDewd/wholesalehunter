﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UpsRateApi
{
    public class RateResponse
    {
        private string RawJson { get; set; }
        private JObject obj { get; set; }
        public string ResponseType { get; set; }
        public  string ResponseStatusCode { get; set; }
        public string ResponseStatusDescription { get; set; }
        public string CustomerContext { get; set; }
        public List<Alert> Alerts { get; set; }
        public string ServiceType { get; set; }
        public List<Alert> RatedShipmentAlerts { get; set; }
        public double BillingWeight { get; set; }
        public string BillingWeightUnits { get; set; }
        public decimal TransportationCharges { get; set; }
        public decimal ServiceOptionsCharges { get; set; }
        public decimal TotalCharges { get; set; }
        public Fault ResponseFault { get; set; }

        public RateResponse(string JSON)
        {
            this.RawJson = JSON;
            Alerts = new List<Alert>();
            RatedShipmentAlerts = new List<Alert>();
            doParse();
        }

        public void doParse()
        {
            obj = JObject.Parse(RawJson);
            
            foreach(var pair in obj)
            {
                ResponseType = pair.Key;
                break;
            }

            if(ResponseType.ToLower()!="rateresponse")
            {
                goFault();
                return;
            }

            var response = obj["RateResponse"]["Response"];

            this.ResponseStatusCode = (string)response["ResponseStatus"]["Code"];
            this.ResponseStatusDescription = (string)response["ResponseStatus"]["Description"];

            var alerts = response["Alert"];
            string code = string.Empty;
            string description= string.Empty;
            foreach (JObject alert in alerts) {

                foreach(JProperty prop in alert.Properties())
                {
                    if (prop.Name == "Code") code = (string)prop.Value;
                    if (prop.Name == "Description") description = (string)prop.Value;
                }
                Alert tmp = new Alert();
                tmp.Code = code;
                tmp.Description = description;
                Alerts.Add(tmp);
            }

            CustomerContext = (string)response["TransactionReference"]["CustomerContext"];

            var ratedshipment = obj["RateResponse"]["RatedShipment"];
            this.ServiceType = (string)ratedshipment["Service"]["Code"];

            var rsalerts = ratedshipment["RatedShipmentAlert"];
            foreach(JObject rsa in rsalerts)
            {
                foreach(JProperty prop in rsa.Properties())
                {
                    if (prop.Name == "Code") code = (string)prop.Value;
                    if (prop.Name == "Description") description = (string)prop.Value;
                }
                Alert tmp = new Alert();
                tmp.Code = code;
                tmp.Description = description;
                RatedShipmentAlerts.Add(tmp);
            }

            double weight;
            if(!double.TryParse((string)ratedshipment["BillingWeight"]["Weight"], out weight))
            {
                weight = 0;
            }
            this.BillingWeight = weight;

            this.BillingWeightUnits = (string)ratedshipment["BillingWeight"]["UnitOfMeasurement"]["Description"];

            decimal transportationcharges;
            if(!decimal.TryParse((string)ratedshipment["TransportationCharges"]["MonetaryValue"], out transportationcharges))
            {
                transportationcharges = 0;
            }
            this.TransportationCharges = transportationcharges;

            decimal serviceoptionscharges;
            if (!decimal.TryParse((string)ratedshipment["ServiceOptionsCharges"]["MonetaryValue"], out serviceoptionscharges))
            {
                serviceoptionscharges = 0;
            }
            this.ServiceOptionsCharges = serviceoptionscharges;

            decimal totalcharges;
            if (!decimal.TryParse((string)ratedshipment["TotalCharges"]["MonetaryValue"], out totalcharges))
            {
                totalcharges = 0;
            }
            this.TotalCharges = totalcharges;
        }



        public void goFault()
        {
            Fault fault = new Fault();
            var response = obj["Fault"];
            fault.FaultCode = (string)response["faultcode"];
            fault.FaultString = (string)response["faultstring"];
            var errors = response["detail"]["Errors"];
            if(errors.Children().Count()>0)
            {
                fault.PrimaryErrorSeverity = (string)errors["ErrorDetail"]["Severity"];
                fault.PrimaryErrorCode = new Alert();
                fault.PrimaryErrorCode.Code = (string)errors["ErrorDetail"]["PrimaryErrorCode"]["Code"];
                fault.PrimaryErrorCode.Description = (string)errors["ErrorDetail"]["PrimaryErrorCode"]["Description"];
            }
            this.ResponseFault = fault;
        }
    }
}
