﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpsRateApi
{

    public class ShipAddress
    {
        public string Name { get; set; }
        public string ID { get; set; }
        public string AddressLine { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }

        public ShipAddress()
        {
            this.Name = string.Empty;
            this.ID = string.Empty;
            this.AddressLine = string.Empty;
            this.City = string.Empty;
            this.State = string.Empty;
            this.Zip = string.Empty;
            this.Country = "US";
        }
    }
}
