﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class CartViewModel : BaseViewModel
    {
        public List<CartListItem> items { get; set; }
        public CartShipping shipping { get; set; }
        public List<CartSuggestedProducts> suggested { get; set; }
        public decimal ShippingAmount { get; set; }
        public string ShippingDisplay { get; set; }
        public decimal TaxAmount { get; set; }
        public string TaxDisplay { get; set; }
        public decimal HazmatAmount { get; set; }
        public string HazmatDisplay { get; set; }
        public bool CartIsEmpty { get; set; }
        public bool AllowedToShip { get; set; }

        public CartViewModel()
        {
            items = new List<CartListItem>();
            suggested = new List<CartSuggestedProducts>();
            AllowedToShip = true;
        }

        public decimal GetCartSubtotal()
        {
            decimal total=0;
            foreach (CartListItem item in items)
            {
                total += (item.Quantity * item.Price);
            }

            return total;
        }
    }
}