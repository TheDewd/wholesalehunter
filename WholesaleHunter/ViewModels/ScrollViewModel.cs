﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class ScrollViewModel
    {
        public List<SearchProduct> Products { get; set; }
        public string Keywords { get; set; }
        public string NextURL { get; set; }

        public ScrollViewModel()
        {
            Products = new List<SearchProduct>();
        }
    }
}