﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        public int CustomerModel { get; set; }
        public List<SearchProduct> Products { get; set; }
        public SearchResultCount ResultCount { get; set; }
        public List<SearchCategory> Category { get; set; }
        public List<SearchBrand> Brands { get; set; }
        public List<Special> Specials { get; set; }
        public List<List<SearchFilter>> Filters { get; set; }
        public HTMLMetaData MetaData;

        public string SortBy { get; set; }
        public string KeyWords { get; set; }
        public string CategorySelected { get; set; }
        public string CategorySelectedName { get; set; }
        public string BrandSelected { get; set; }
        public string BrandSelectedName { get; set; }
        public bool InStockSelected { get; set; }
        public bool NewItemsSelected { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public int StartRow { get; set; }
        public int EndRow { get; set; }
        public Dictionary<int, string> FilterGroupNames { get; set; }
        public string FiltersJSON { get; set; }
        public Dictionary<string, string> FiltersSelected { get; set; }
        public string Dept { get; set; }
        public string ScrollUrl { get; set; }

        public Dictionary<string, string> SortByListStyle { get; set; }
        public Dictionary<string, string> SortByAnchorClass { get; set; }

        public SearchViewModel()
        {
            KeyWords = "";
            Products = new List<SearchProduct>();
            ResultCount = new SearchResultCount();
            Category = new List<SearchCategory>();
            Brands = new List<SearchBrand>();
            Specials = new List<Special>();
            Filters = new List<List<SearchFilter>>();
            FiltersSelected = new Dictionary<string, string>();
            FilterGroupNames = new Dictionary<int, string>();
            BrandSelectedName = "";
            CategorySelectedName = "";

            SortByListStyle = new Dictionary<string, string>();
            SortByListStyle.Add("newest", "style=\"display:none;\"");
            SortByListStyle.Add("price low to high", "style=\"display:none;\"");
            SortByListStyle.Add("price high to low", "style=\"display:none;\"");
            SortByListStyle.Add("popularity", "style=\"display:none;\"");
            SortByListStyle.Add("price per unit", "style=\"display:none;\"");
            SortByListStyle.Add("clearance", "style=\"display:none;\"");

            SortByAnchorClass = new Dictionary<string, string>();
            SortByAnchorClass.Add("newest", "a_sortby");
            SortByAnchorClass.Add("price low to high", "a_sortby");
            SortByAnchorClass.Add("price high to low", "a_sortby");
            SortByAnchorClass.Add("popularity", "a_sortby");
            SortByAnchorClass.Add("price per unit", "a_sortby");
            SortByAnchorClass.Add("clearance", "a_sortby");
        }
    }
}