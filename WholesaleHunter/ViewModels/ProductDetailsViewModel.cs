﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class ProductDetailsViewModel : BaseViewModel
    {
        public int ProductID { get; set; }
        public List<Special> Specials { get; set; }
        public ProductDetails Details { get; set; }
        public List<ProductDetailsImages> Images { get; set; }
        public List<ProductDetailsSpecs> Specs { get; set; }
        //public List<ProductDetailsA> DetailsA { get; set; }
        public List<Product> SuggestedProducts { get; set; }
        public List<PricePoints> Prices { get; set; }
        public string MainImage { get; set; }
        public string Email { get; set; }
        public string SourceID { get; set; }

        public bool IsFromSearch { get; set; }

        public ProductDetailsViewModel()
        {
            this.Specials = new List<Special>();
            this.Images = new List<ProductDetailsImages>();
            this.Specs = new List<ProductDetailsSpecs>();
            this.SuggestedProducts = new List<Product>();
        }
    }
}