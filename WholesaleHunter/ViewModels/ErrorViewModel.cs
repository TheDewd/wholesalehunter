﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class ErrorViewModel : BaseViewModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public bool ShowBackToPaymentButton { get; set; }
        public bool ShowBackToMyInfoButton { get; set; }
        public bool ShowOkButton { get; set; }

        public ErrorViewModel()
        {
            this.ShowBackToPaymentButton = false;
            this.ShowBackToMyInfoButton = false;
            this.ShowOkButton = false;
        }
    }
}