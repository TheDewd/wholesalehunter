﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class IndexViewModel : BaseViewModel
    {
        public List<Special> Specials { get; set; }
        public List<News> News { get; set; }

        public List<Product> TopSellingInitial { get; set; }
        public List<Product> TopSelling { get; set; }
        public List<Product> NewProducts { get; set; }
        public List<Product> Clearance { get; set; }
        public List<Product> RecentlyViewed { get; set; }
        public List<Product> Guns { get; set; }
        public List<Product> Ammunition { get; set; }
        public List<Product> Reloading { get; set; }
        public bool ShowCartOnLoad { get; set; }


        public IndexViewModel()
        {
            this.Specials = new List<Special>();
            this.News = new List<News>();
            this.TopSellingInitial = new List<Product>();
            this.TopSelling = new List<Product>();
            this.NewProducts = new List<Product>();
            this.Clearance = new List<Product>();
            this.RecentlyViewed = new List<Product>();
            this.Guns = new List<Product>();
            this.Ammunition = new List<Product>();
            this.Reloading = new List<Product>();
        }
    }
}