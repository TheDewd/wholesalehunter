﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class SiteMapViewModel : BaseViewModel
    {
        public List<SiteCategory> SiteCategories { get; set; }
        public List<SiteCategory> DistinctSiteCategories { get; set; }
        public List<Special> Specials { get; set; }
        public List<SitemapProduct> Products {get;set;}
    }
}