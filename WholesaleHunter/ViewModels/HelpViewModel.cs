﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class HelpViewModel : BaseViewModel
    {
        public List<Special> Specials { get; set; }
        public List<HelpFile> HelpFile { get; set; }
        public List<HelpFile> HelpCategories { get; set; }
        public List<HelpFile> FAQ { get; set; }
        public List<HelpFile> HelpTopics { get; set; }
    }
}