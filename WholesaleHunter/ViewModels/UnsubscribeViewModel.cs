﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.ViewModels
{
    public class UnsubscribeViewModel : BaseViewModel
    {
        public string Email { get; set; }
        public int Status { get; set; }
    }
}