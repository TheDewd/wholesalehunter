﻿function updateContactPreferences() {
    var getsNewsLetter = true;
    if (document.getElementById("newsletter").checked == true)
    {
        getsNewsLetter = true;
    }
    else
    {
        getsNewsLetter = false;
    }

    var contactPreferences = {
        CustomerID: $("#customerID").val(),
        Phone: $("#phone").val(),
        Email: $("#email").val(),
        GetNewsLetter: getsNewsLetter
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "/api/AJax/UpdateContactPreferences",
        data: JSON.stringify(contactPreferences),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data == "ok") {
                showMyAccount(3);
            }
            else {
                alert("An error was encountered attempting to update the contact preferences. Please try again and if the error persists please contact our customer service department.");
                return false;
            }
        }
    });
};