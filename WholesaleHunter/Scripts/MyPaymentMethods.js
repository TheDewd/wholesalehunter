﻿function deletePaymentMethod(divCounter) {
    if (confirm("Are you sure you want to delete this payment method?"))
    {
        var customerPaymentMethodID = "#myPaymentMethodIDVal" + divCounter
        var currentPaymentMethod = {
            CustomerPaymentMethodID: $(customerPaymentMethodID).val()
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "/api/AJax/DeletePaymentMethod",
            data: JSON.stringify(currentPaymentMethod),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data == "ok") {
                    showMyAccount(2);
                }
                else {
                    alert("An error was encountered attempting to delete the address. Please try again and if the error persists please contact our customer service department.");
                    return false;
                }
            }
        });
    }
};

function makePrimaryPaymentMethod(divCounter) {
    if (confirm("Are you sure you want to make this the primary payment method?"))
    {
        var customerPaymentMethodID = "#myPaymentMethodIDVal" + divCounter
        var customerID = "#customerID"
        var currentPaymentMethod = {
            CustomerPaymentMethodID: $(customerPaymentMethodID).val(),
            CustomerID: $(customerID).val()
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "/api/AJax/MakePrimaryPaymentMethod",
            data: JSON.stringify(currentPaymentMethod),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data == "ok") {
                    showMyAccount(2);
                }
                else {
                    alert("An error was encountered attempting to delete the address. Please try again and if the error persists please contact our customer service department.");
                    return false;
                }
            }
        });
    }
};


function showNewPaymentMethod() {
    $("#newPaymentMethod").show();
    $("#addNew").hide();
};

/*
function addPaymentMethod() {
    
    var exMonth = $("#newExpirationMonth").val();
    var exYear = $("#newExpirationYear").val();
    exMonth = parseInt(exMonth);
    exMonth = exMonth + 1;
    //alert(month + year );
    var expirationDate = new Date(exYear, exMonth, 0); //Get the last day of the month
    var currentDate= new Date();

    //alert(expirationDate);

    var customerID = "#customerID"
    var cardNumber = $("#newCardNumber").val();
    var lastFour = cardNumber.substr(cardNumber.length - 4);
    var authorizeNETToken = "";    

    var currentPaymentMethod = {
        CustomerID: $(customerID).val(),
        LastFour: lastFour,
        ExpirationDate: expirationDate,
        AuthorizeNETToken: authorizeNETToken
    }

    $.ajax({
        type: "POST",
        url: "/api/AJax/AddNewPaymentMethod",
        data: JSON.stringify(currentPaymentMethod),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data == "ok") {
                showMyAccount():
            }
            else {
                alert("An error was encountered attempting to add this payment method. Please try again and if the error persists please contact our customer service department.");
                return false;
            }
        }
    });
}; */

function showNormal() {
    $("#newPaymentMethod").hide();
    $("#addNew").show();
};
   


