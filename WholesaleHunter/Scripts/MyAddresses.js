﻿function displayEdit(divNum) {

    var labelAddressName = "#addressNameLabel" + divNum;
    var editAddressName = "#addressNameEdit" + divNum;
    var editAddress = "#addressEdit" + divNum;
    var labelAddress = "#addressLabel" + divNum
    var editAddress2 = "#address2Edit" + divNum
    var labelAddress2 = "#address2Label" + divNum
    var labelCity = "#cityLabel" + divNum
    var editCity = "#cityEdit" + divNum
    var editButtonGroup = "#editButtonGroup" + divNum
    var updateButtonGroup = "#updateButtonGroup" + divNum

    $(labelAddressName).hide();
    $(editAddressName).show();
    $(labelAddress).hide();
    $(editAddress).show();
    $(labelAddress2).hide();
    $(editAddress2).show();
    $(labelCity).hide();
    $(editCity).show();
    $(editButtonGroup).hide();
    $(updateButtonGroup).show();
};

function displayNormal(divNum) {

    var labelAddressName = "#addressNameLabel" + divNum;
    var editAddressName = "#addressNameEdit" + divNum;
    var editAddress = "#addressEdit" + divNum;
    var labelAddress = "#addressLabel" + divNum
    var editAddress2 = "#address2Edit" + divNum
    var labelAddress2 = "#address2Label" + divNum
    var labelCity = "#cityLabel" + divNum
    var editCity = "#cityEdit" + divNum
    var editButtonGroup = "#editButtonGroup" + divNum
    var updateButtonGroup = "#updateButtonGroup" + divNum

    $(labelAddressName).show();
    $(editAddressName).hide();
    $(labelAddress).show();
    $(editAddress).hide();
    $(labelAddress2).show();
    $(editAddress2).hide();
    $(labelCity).show();
    $(editCity).hide();
    $(editButtonGroup).show();
    $(updateButtonGroup).hide();
};


function updateAddress(divCounter) {
    var customerAddressID = "#customerAddressIDVal" + divCounter
    var addressName="#addressNameVal" + divCounter
    var address1 = "#address1Val" + divCounter
    var address2 = "#address2Val" + divCounter
    var city = "#cityVal" + divCounter
    var state = "#stateVal" + divCounter
    var zip = "#zipCodeVal" + divCounter
  
    var currentAddress = {
        CustomerAddressID: $(customerAddressID).val(),
        AddressName: $(addressName).val(),
        Address1: $(address1).val(),
        Address2: $(address2).val(),
        City: $(city).val(),
        State: $(state).val(),
        ZIPCode: $(zip).val()
     
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "/api/AJax/UpdateAddress",
        data: JSON.stringify(currentAddress),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data == "ok") {
                showMyAccount(1);
            }
           else {
                alert("An error was encountered attempting to update the address. Please try again and if the error persists please contact our customer service department.");
                return false;
            }
        }
    });
    $("#myaccountdialog").hide();
    $("#myaccountdialog").html('');;
    $("#myaccountdialog").dialog('close');

}

function deleteAddress(divCounter) {
    
    if (confirm("Are you sure you want to delete this address?"))
    {
        var customerAddressID = "#customerAddressIDVal" + divCounter
        var currentAddress = {
            CustomerAddressID: $(customerAddressID).val()
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "/api/AJax/DeleteAddress",
            data: JSON.stringify(currentAddress),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data == "ok") {
                    showMyAccount(1);
                }
                else {
                    alert("An error was encountered attempting to delete the address. Please try again and if the error persists please contact our customer service department.");
                    return false;
                }
            }
        });
    }
};

function makePrimary(divCounter) {

    if (confirm("Are you sure you want to make this the primary address?")) {
        var customerAddressID = "#customerAddressIDVal" + divCounter
        var customerID = "#customerIDVal" + divCounter

        var currentAddress = {
            CustomerAddressID: $(customerAddressID).val(),
            CustomerID: $(customerID).val()
        }

        $.ajax({
            type: "POST",
            cache: false,
            url: "/api/AJax/MakePrimary",
            data: JSON.stringify(currentAddress),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data == "ok") {
                    showMyAccount(1);
                }
                else {
                    alert("An error was encountered attempting to make this the primary address. Please try again and if the error persists please contact our customer service department.");
                    return false;
                }
            }
        });
    }


};

function addNewAddress() {
    
    var addressName = "#newAddressName" 
    var address1 = "#newAddress1" 
    var address2 = "#newAddress2" 
    var city = "#newCity" 
    var state = "#newState" 
    var zip = "#newZipCode" 
    var customer="#customerID"

    var currentAddress = {
        CustomerID: $(customer).val(),
        AddressName: $(addressName).val(),
        Address1: $(address1).val(),
        Address2: $(address2).val(),
        City: $(city).val(),
        State: $(state).val(),
        ZIPCode: $(zip).val()

    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "/api/AJax/AddNewAddress",
        data: JSON.stringify(currentAddress),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data == "ok") {
                showMyAccount(1);
            }
            else {
                alert("An error was encountered attempting to add this address. Please try again and if the error persists please contact our customer service department.");
                return false;
            }
        }
    });
}

function showNewAddress() {
    $("#newAddress").show();
    $("#addNew").hide();
};

function cancelNewAddress() {
    $("#newAddress").hide();
    $("#addNew").show();
};