﻿function buyAgain(CustomerID, ProductID, Price, Qty) {
    var custid;
    if (isNaN(CustomerID)) {
        custid = 0;
    }
    else {
        custid = parseInt(CustomerID);
    }

    var obj =
        {
            CustomerID: custid,
            ProductID: ProductID,
            Price: Price,
            Quantity: Qty
        }
      

    $.ajax({
        type: "POST",
        cache: false,
        url: "/api/AJax/AddToCart",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data == "Error") {
                showMessage("An error was encountered adding this item to your cart. If this message persists please contact the system administrator");
            }
            else {
                $("#myaccountdialog").dialog().dialog('close');
                $("#cartQty").html("(?)");
                showCart();
            }
        }
    });

};


