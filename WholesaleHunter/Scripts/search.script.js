﻿function filterClearExtra(FilterGroupName) {
    var objFilters = new Object();
    //get all the filters and their selected values
    $('input[id^="FilterValue_"]').each(function (index, element) {
        if (element.value != "") {
            if (element.value.indexOf(FilterGroupName) == 0) {
                element.value = "";
            }
        }
    });
    submitSearch();
}

function selectFilter(filterGroup, filterName, filterValue) {
    //alert(filterGroup + ", " + filterName + ", " + filterValue);
    $("#FilterValue_" + filterGroup).val(filterName + "::" + filterValue);
    submitSearch();
}

function submitSearch() {
    setFilter();
    var data = "/Search/Submit";
    data += "?CategoryID=" + $("#categoryselected").val();
    data += "&MinPrice=" + $("#minprice").val();
    data += "&MaxPrice=" + $("#maxprice").val();
    data += "&BrandID=" + $("#brandselected").val();
    data += "&InStockOnly=" + $("#instockselected").val();
    data += "&NewOnly=" + $("#newitemsselected").val();
    data += "&Keywords=" + encodeURIComponent($("#keywords").val());
    data += "&SortBy=" + $("#sortbyselected").val();
    data += "&StartRow=" + $("#startrow").val();
    data += "&EndRow=" + $("#endrow").val();
    if ($("#dept").val() != "") {
        data += "&Dept=" + $("#dept").val();
    }

    var filters = $("#filtersselected").val();
    filters = filters.replace("&", "%26");

    data += "&filtersselected=" + filters;
    //alert(data);
    location.href = data;
}

function setFilter() {
    var objFilters = new Object();
    var cnt = 0;
    //get all the filters and their selected values
    $('input[id^="FilterValue_"]').each(function (index, element) {
        if (element.value != "") {
            var fltr = element.value.split("::");
            objFilters[fltr[0]] = fltr[1];
            cnt++;
        }
    });

    if(cnt>0) {
        $("#filtersselected").val(JSON.stringify(objFilters));
    }
    else
    {
        $("#filtersselected").val('');
    }
}

function addToCart(prodid) {

    if ($("#qty_" + prodid).val() < 1) {
        alert("The quantity must be greater than zero.");
        $("#qty_" + prodid).val("1");
        return false;
    }

    /*
    $("#dialog").dialog({
        dialogClass: "no-close",
        position: { my: "center top", at: "center-75 top+80", of: window }
    });

    $("#dialog").html('The item is being added to your cart.');
    $("#dialog").show();
    showPageMask(false);
    setTimeout(function () { $("#dialog").hide(); $("#pagemask").hide(); }, 1000);
    */

    var obj =
        {
            CustomerID: $("#customerid").val(),
            ProductID: prodid,
            Price: $("#price_" + prodid).val(),
            Quantity: $("#qty_" + prodid).val()
        }

    $.ajax({
        type: "POST",
        cache: false,
        url: "/api/AJax/AddToCart",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.toLowerCase().startsWith('error')) {
                showMessage(data);
            } else {
                var obj = JSON.parse(data);
                $("#cartQty").html(obj.TotalQuantity);
                $("#cartTotalPrice").html(obj.TotalItemPrice);
                //$("#cartTarget").fadeIn(500).fadeOut(500);
                updateCheckout();
                showCart();
            }
        }
    });
}

function showMessage(message) {
    $("#pagemask").show();
    $("#divMessage").html(message);
    $("div#message").dialog({
        position: { my: "center top", at: "center-360 top+20", of: window }
    });
    return false;
}

function closeMessage() {
    $("#pagemask").hide();
    $("div#message").dialog('close');
}

function hidePricePoints() {
    $("#pricepoints").hide();
}

function getPricePoints(prodid) {
    $("#currentProductID").val(prodid);
    var obj =
    {
        ID: prodid
    }

    $.ajax({
        type: "POST",
        cache: false,
        url: "/api/AJax/GetPricePoints",
        data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //get the x and y of the span
            var prodid = $("#currentProductID").val();
            var label = $("#qtyprices_" + prodid);
            var posit = label.position();
            var offset = label.offset();
            var divPricePoints = $("#pricepoints");
            var outer = $("div#outer");
            var outerPosition = outer.position();
            var outerOffset = outer.offset();
            divPricePoints.html(data);
            divPricePoints.css({ top: offset.top - outerOffset.top, left: (offset.left - outerOffset.left)+260 });
            divPricePoints.show();
            //console.log("posit top,left : " + posit.top + ", " + posit.left + "\noffset top, left: " + offset.top + ", " + offset.left);
            //console.log("outerPosition top,left : " + outerPosition.top + ", " + outerPosition.left + "\nouterOffset top, left: " + outerOffset.top + ", " + outerOffset.left);
            setTimeout(hidePricePoints, 10000);
        }
    });
}

function manualQtyChange(prodid) {
    //make sure the qty is an integer value
    var qty = $("#qty_" + prodid).val();

    if (isNaN(qty)) {
        alert("The quantity must be a numeric value.");
        $("#qty_" + prodid).val("1");
        return false;
    }

    if (parseInt(qty) <= 0) {
        alert("The quantity must be a positive numeric value.");
        $("#qty_" + prodid).val("1");
        return false;
    }

    var intQty = parseInt(qty);
    if (qty != intQty) {
        qty = intQty;
        $("#qty_" + prodid).val(intQty);
    }
    //now make sure they aren't ordering more than is available
    var qtyTotal = $("#qty_total_" + prodid).val();
    if (parseInt(qty) > parseInt(qtyTotal)) {
        if (confirm("There are only " + qtyTotal + " units available. Add this quantity to the cart?")) {
            $("#qty_" + prodid).val(qtyTotal);
            addToCart(prodid);
        } else {
            $("#qty_" + prodid).val(qtyTotal);
        }
    }
}

function increment_qty(prodid) {
    var button = $("#increment_" + prodid);
    if (!button.hasClass("disabledbutton")) {
        var qty = $("#qty_" + prodid).val();
        $("#qty_" + prodid).val(++qty);
        var qtyAtPrice = $("#qty_at_price_" + prodid).val();
        if (parseInt(qty) > parseInt(qtyAtPrice)) {
            //turn box red
            $("#qty_" + prodid).css("color", "#ff0000");
        }
        else {
            $("#qty_" + prodid).css("color", "#000000");
        }
        var qtyTotal = $("#qty_total_" + prodid).val();
        if (parseInt(qty) >= parseInt(qtyTotal)) {
            //disable the increment button
            button.addClass("disabledbutton");
        }
        if (qty > 0) {
            //reneable the decrement button
            $("#decrement_" + prodid).removeClass("disabledbutton");
        }
    }
}

function decrement_qty(prodid) {
    var button = $("#decrement_" + prodid);
    if (!button.hasClass("disabledbutton")) {
        var qty = $("#qty_" + prodid).val();
        $("#qty_" + prodid).val(--qty);


        var qtyAtPrice = $("#qty_at_price_" + prodid).val();
        var qtyTotal = $("#qty_total_" + prodid).val();
        if (qty <= qtyAtPrice) {
            //turn box black
            $("#qty_" + prodid).css("color", "#000000");
        }
        if (qty < qtyTotal) {
            //re-enable the increment button
            $("#increment_" + prodid).removeClass("disabledbutton");
        }
        if (qty <= 0) {
            //disable the increment button
            button.addClass("disabledbutton");
        }
    }
}

/*
$(document).ready(function () {
    $(".scroll").jscroll({
        nextSelector: 'a.scroll:last'
    });
});
*/

function removeKeyword(kw) {
    currentKeywords = $("#keywords").val();
    currentKeywords = currentKeywords.replace(kw, "");
    currentKeywords = currentKeywords.replace(/  /g, " "); //now replace any double spaces with a single space
    currentKeywords = currentKeywords.trim();
    $("#keywords").val(currentKeywords);
    submitSearch();
}


function addKeywords() {
    var keywordsin = $("#keywordsearch").val();
    $("#keywords").val(keywordsin);
    submitSearch();
}


function filterClear(filter) {
    switch (filter) {
        case "category":
            $("#categoryselected").val("0");
            break;
        case "brand":
            $("#brandselected").val("0");
            break;
        case "price":
            $("#minprice").val("0");
            $("#maxprice").val("999999999999999");
            break;
        case "instock":
            $("#instockselected").val("False");
            break;
        case "newitems":
            $("#newitemsselected").val("False");
            break;
    }
    submitSearch();
}

function setPriceRange() {
    var min = $("#pricerangemin").val();
    var max = $("#pricerangemax").val();

    min = min.replace("$", '');
    max = max.replace("$", '');



    if (isNaN(min)) {
        alert('The minimum price must be a numeric value.');
        return false;
    }

    if (isNaN(max)) {
        alert('The maximum price must be a numeric value.');
        return false;
    }

    if (parseFloat(min) >= parseFloat(max)) {
        alert('The maximum price must be greater than the minimum price.');
        return false;
    }

    if (min == '') {
        min = 0;
    }

    if (max == '') {
        max = 999999999999999;
    }
    $("#minprice").val(min);
    $("#maxprice").val(max);
    submitSearch();
}

function selectBrand(brandid) {
    $("#brandselected").val(brandid);
    submitSearch();
}

function chooseCategory(categoryid) {
    $("#categoryselected").val(categoryid);
    submitSearch();
}

//this setups up an event trigger to close the sortby menu if the user
//moves the mouse out of the menu.
$(document).ready(function () {
    $("div.sortbymenu").mouseleave(function () {
        if (!$(".a_sortby").hasClass("selected")) {
            //nothing was selected, so select the previously selected one
            var last_selected = "";
            switch ($("#sortbyselected").val().toLowerCase()) {
                case "newest":
                    last_selected = "sortbyNewest";
                    break;
                case "price low to high":
                    last_selected = "sortbyLow";
                    break;
                case "price high to low":
                    last_selected = "sortbyHigh";
                    break;
                case "popularity":
                    last_selected = "sortbyPopularity";
                    break;
                case "price per unit":
                    last_selected = "sortbyPPU";
                    break;
                case "clearance":
                    last_selected = "sortbyClearance";
                    break;
            }
            sortby(last_selected, false);
        }
    });
});

function sortby(sort_criteria, doSubmit) {
    var id;
    var sortvalue;
    if (doSubmit === undefined) {
        doSubmit = true;
    }


    switch (sort_criteria) {
        case "sortbyNewest":
            sortvalue = "Newest";
            break;
        case "sortbyLow":
            sortvalue = "Price Low to High";
            break;
        case "sortbyHigh":
            sortvalue = "Price High to Low"
            break;
        case "sortbyPopularity":
            sortvalue = "Popularity";
            break;
        case "sortbyPPU":
            sortvalue = "Price per Unit";
            break;
        case "sortbyClearance":
            sortvalue = "Clearance";
            break;
    }

    //alert("sort_criteria: " + sort_criteria + "\nsortvalue: "+sortvalue+"\ndoSubmit: " + doSubmit);

    if ($("#a_" + sort_criteria).hasClass("selected")) {
        //the sortby which was clicked is visible
        //so this click is just to display the menu
        $("#a_" + sort_criteria).removeClass("selected")
        $(".sortby").show();
    }
    else {
        $(".a_sortby").removeClass('selected');
        $("#a_" + sort_criteria).addClass("selected")
        $(".sortby").hide();
        $("#" + sort_criteria).show();
        $("#sortbyselected").val(sortvalue);
        if (doSubmit) {
            submitSearch();
        }
    }
}

function toggleCheckbox(id) {
    var instock = false;
    if (id.toLowerCase().indexOf('stock') > 0) {
        instock = true;
    }

    $("#" + id).toggleClass("checkbox_checked");
    if ($("#" + id).hasClass("checkbox_checked")) {
        if (instock) {
            $("#instockselected").val("True");
        } else {
            $("#newitemsselected").val("True");
        }
    }
    else {
        if (instock) {
            $("#instockselected").val("False");
        } else {
            $("#newitemsselected").val("False");
        }
    }
    submitSearch();
}

function toggleFilterHeader(filterGroup) {
    if ($("div#Twistie_" + filterGroup).hasClass("twistieRight")) {
        //filter is currenlty collapsed
        $("ul#filterGroup_" + filterGroup + " li.filterAlwaysVisible").removeClass('filterHidden');
        $("li#liFilterLess_" + filterGroup).hide();
        $("div#Twistie_" + filterGroup).removeClass("twistieRight");
        $("li#liFilterMore_" + filterGroup).show();
    }
    else
    {
        //filter is currently expanded
        $("ul#filterGroup_" + filterGroup + " li").addClass('filterHidden');
        $("li#liFilterLess_" + filterGroup).hide();
        $("div#Twistie_" + filterGroup).addClass("twistieRight");
        $("li#liFilterMore_" + filterGroup).hide();


    }
}

function toggleCategories() {
    //see if the brands are currently displayed
    if ($("#categoriesTwistie").hasClass("twistieRight"))
    {
        $("#categoriesTwistie").removeClass("twistieRight");
        $("ul#categories li.filterAlwaysVisible").removeClass("filterHidden");
        $("#liCategoriesLess").hide();
        $("#liCategoriesMore").show();
    }
    else
    {
        $("#categoriesTwistie").addClass("twistieRight");
        $("ul#categories li").addClass("filterHidden");
        $("#liCategoriesLess").hide();
        $("#liCategoriesMore").hide();
    }
}

function toggleCategoriesMore() {
    if ($("#liCategoriesLess").is(":visible")) {
        //categories is fully expanded
        $("ul#categories li:not(.filterAlwaysVisible)").addClass('filterHidden');
        $("#liCategoriesLess").hide();
        $("#liCategoriesMore").show();
    }
    else
    {
        //categories is only showing top 10
        $("ul#categories li:not(.filterAlwaysVisible)").removeClass('filterHidden');
        $("#liCategoriesLess").show();
        $("#liCategoriesMore").hide();
    }
}

function toggleBrandsMore() {
    if ($("#liBrandsLess").is(":visible")) {
        //brands is fully expanded
        $("ul#brands li:not(.filterAlwaysVisible)").addClass('filterHidden');
        $("#liBrandsLess").hide();
        $("#liBrandsMore").show();
    }
    else {
        //brands is showing top 5
        $("ul#brands li:not(.filterAlwaysVisible)").removeClass('filterHidden');
        $("#liBrandsLess").show();
        $("#liBrandsMore").hide();
    }
}

function toggleBrands() {
    //see if the brands are currently displayed
    if ($("#brandsTwistie").hasClass("twistieRight")) {
        $("#brandsTwistie").removeClass("twistieRight");
        $("ul#brands li.filterAlwaysVisible").removeClass("filterHidden");
        $("#liBrandsLess").hide();
        $("#liBrandsMore").show();
    }
    else
    {
        $("#brandsTwistie").addClass("twistieRight");
        $("ul#brands li").addClass("filterHidden");
        $("#liBrandsMore").hide();
        $("#liBrandsLess").hide();
    }
}

function toggleFiltersMore(filterGroup) {
    if ($("#liFilterLess_" + filterGroup).is(":visible")) {
        $("ul#filterGroup_" + filterGroup + "  li.filterVisible").addClass("filterHidden");
        $("ul#filterGroup_" + filterGroup + "  li.filterHidden").removeClass("filterVisible");
        $("#liFilterLess_" + filterGroup).hide();
        $("#liFilterMore_" + filterGroup).show();
    }
    else {
        $("ul#filterGroup_" + filterGroup + "  li.filterHidden").addClass("filterVisible");
        $("ul#filterGroup_" + filterGroup + "  li.filterVisible").removeClass("filterHidden");
        $("#liFilterLess_" + filterGroup).show();
        $("#liFilterMore_" + filterGroup).hide();
    }
}

function unescapeHtml(data) {
    return data
         .replace(/&amp;/g, "&")
         .replace(/&lt;/g, "<")
         .replace(/&gt;/g, ">")
         .replace(/&quot;/g, "\"")
         .replace(/&#039;/g, "'");
}

function formatMoney(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];

    var x2 = x.length > 1 ? '.' + x[1] : '.00';

    if (x.length > 1) {
        //the number has a decimal component
        x2 = x[1] + '00';  //make sure it's at least 2 digits long
        x2 = '.' + x2.substr(0, 2);
    } else {
        //the number does not have a decimal component
        x2 = '.00';
    }

    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    if (x1 == "") {
        x1 = "0";
    }
    return "$" + x1 + x2;
}
