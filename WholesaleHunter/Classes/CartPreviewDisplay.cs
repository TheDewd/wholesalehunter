﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using WholesaleHunter.Models;

namespace WholesaleHunter.Classes
{
    public class CartPreviewDisplay
    {
        public string TotalQuantity { get; set; }
        public string TotalItemPrice { get; set; }

        public CartPreviewDisplay(CartPreview cart)
        {
            NumberFormatInfo nfi = CultureInfo.CurrentCulture.NumberFormat;
            nfi = (NumberFormatInfo)nfi.Clone();

            NumberFormatInfo nfiNoDecimal = CultureInfo.CurrentCulture.NumberFormat;
            nfiNoDecimal = (NumberFormatInfo)nfiNoDecimal.Clone();
            nfiNoDecimal.CurrencyDecimalDigits = 0;
            nfiNoDecimal.CurrencySymbol = "";

            this.TotalQuantity = String.Format(nfiNoDecimal, "({0:C})", cart.TotalQuantity);
            this.TotalItemPrice = String.Format(nfi, "{0:C}", cart.TotalItemPrice);
        }
    }
}