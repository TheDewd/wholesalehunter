﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WholesaleHunter.Classes
{
    public class TrackingURL
    {
        public string GetTrackingURL(string Shipper)
        {
            if(Shipper==null)
            {
                return "";
            }
            //This method accepts a shipper name and returns a URL based on that name
            var trackingURL="";
            Shipper = Shipper.ToUpper();

            switch (Shipper)
            {
                case "UPS":
                    {
                        trackingURL = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=";
                        break;
                    }

                case "USPS":
                    {
                        trackingURL = "https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=";
                        break;
                    }
            }


            return trackingURL;

        }
    }
}